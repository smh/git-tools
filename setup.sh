basePath=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/git/2.26.2-x86_64-centos7/

export PATH=${basePath}/bin:${PATH}
export MANPATH=${basePath}/docs/man:${MANPATH}
export GIT_EXEC_PATH=${basePath}/libexec/git-core/
export GIT_TEMPLATE_DIR=${basePath}/share/git-core/templates
